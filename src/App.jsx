import React from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';

import { Header } from './components/Header';
import { Main } from './components/Main';
import { Footer } from './components/Footer';
import { sendRequest } from './helpers/sendRequest';

function App() {
    const [listItems, setListItems] = React.useState([]);
    const [favorites, setFavorites] = React.useState([]);
    const [cart, setCart] = React.useState([]);
    const [qtyFavorites, setQtyFavorites] = React.useState(favorites.length);
    const [qtyCart, setQtyCart] = React.useState(cart.length);

    React.useEffect(() => {
        sendRequest('/data.json').then(data => setListItems(data));

        const savedFavorites = localStorage.getItem('favorites');
        const savedCart = localStorage.getItem('cart');

        if (savedFavorites) {
            setFavorites(JSON.parse(savedFavorites));
        }

        if (savedCart) {
            setCart(JSON.parse(savedCart));
        }
    }, []);

    React.useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
        setQtyFavorites(favorites.length);
    }, [favorites]);

    React.useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
        setQtyCart(cart.length);
    }, [cart]);

    return (
        <div className="app-wrapper">
            <Header qtyFavorites={qtyFavorites} qtyCart={qtyCart} />
            <Routes>
                <Route
                    path="*"
                    element={
                        <Main
                            listItems={listItems}
                            favorites={favorites}
                            setFavorites={setFavorites}
                            cart={cart}
                            setCart={setCart}
                        />
                    }
                />
            </Routes>
            <Footer />
        </div>
    );
}

export default App;
