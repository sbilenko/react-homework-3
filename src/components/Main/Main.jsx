import React from 'react';
import PropTypes from 'prop-types';
import { Routes, Route } from 'react-router-dom';

import { ListItems } from './ListItems';
import { Favorites } from '../Favorites';
import { Cart } from '../Cart';
import { NotPage } from '../NotPage';

import './Main.scss';

function Main({ listItems, favorites, setFavorites, cart, setCart }) {
    const addToFavorites = article => {
        if (!favorites.includes(article)) {
            setFavorites([...favorites, article]);
        } else {
            const indexArticle = favorites.indexOf(article);
            favorites.splice(indexArticle, 1);
            setFavorites([...favorites]);
        }
    };

    const addToCart = article => {
        if (!cart.includes(article)) {
            setCart([...cart, article]);
        }
    };
    return (
        <div className="main__wrapper">
            <div className="container">
                <div className="main">
                    <Routes>
                        <Route
                            path="/"
                            element={
                                <ListItems
                                    listItems={listItems}
                                    favorites={favorites}
                                    cart={cart}
                                    addToFavorites={addToFavorites}
                                    addToCart={addToCart}
                                />
                            }
                        />
                        <Route
                            path="/favorites"
                            element={
                                <Favorites listItems={listItems} favorites={favorites} setFavorites={setFavorites} />
                            }
                        />
                        <Route path="/cart" element={<Cart listItems={listItems} cart={cart} setCart={setCart} />} />
                        <Route path="*" element={<NotPage />} />
                    </Routes>
                </div>
            </div>
        </div>
    );
}

Main.propTypes = {
    listItems: PropTypes.array,
    favorites: PropTypes.array,
    cart: PropTypes.array,
    addToFavorites: PropTypes.func,
    addToCart: PropTypes.func,
};

export default Main;
